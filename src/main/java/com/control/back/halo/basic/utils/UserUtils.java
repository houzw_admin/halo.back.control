package com.control.back.halo.basic.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.control.back.halo.basic.commons.Constants;
import com.control.back.halo.basic.entity.User;

public class UserUtils {

    public static String getCurrentUserName() {
        return getCurrentUser().getUsername();
    }

    public static HttpSession getCurrentSession() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return request.getSession();
    };

    public static User getCurrentUser() {
        return (User) getCurrentSession().getAttribute(Constants.USER_SESSION_ID);
    }

    public static void setCurrentUser(User user) {
        getCurrentSession().setAttribute(Constants.USER_SESSION_ID, user);
    }

}
