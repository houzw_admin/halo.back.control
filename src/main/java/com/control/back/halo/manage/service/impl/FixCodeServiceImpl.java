package com.control.back.halo.manage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.control.back.halo.basic.dao.IBaseDao;
import com.control.back.halo.basic.service.impl.BaseServiceImpl;
import com.control.back.halo.manage.dao.IFixCodeDao;
import com.control.back.halo.manage.entity.FixCode;
import com.control.back.halo.manage.service.IFixCodeService;

@Service
public class FixCodeServiceImpl extends BaseServiceImpl<FixCode, Long> implements IFixCodeService {

    @Autowired
    private IFixCodeDao fixCodeDao;

    @Override
    public List<FixCode> getFixCodeByCodeType(Integer codeType) {
        return fixCodeDao.findByCodeType(codeType);
    }

    @Override
    public FixCode getFixCodeByCode(Integer code) {
        return fixCodeDao.findByCode(code);
    }

    @Override
    public FixCode getFixCodeByCodeAndType(Integer codeType, Integer code) {
        return fixCodeDao.findByCodeTypeAndCode(codeType, code);
    }

    @Override
    public IBaseDao<FixCode, Long> getBaseDao() {
        return this.fixCodeDao;
    }

}
